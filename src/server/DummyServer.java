package server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import com.sun.xml.internal.bind.v2.WellKnownNamespace;

public class DummyServer {
	public static void start() throws IOException {
		ServerSocket welcomeSocket = null;
		String clientSentence;
		try {
			welcomeSocket = new ServerSocket(6789);
			while (true) {
				Socket connectionSocket = welcomeSocket.accept();
				BufferedReader inFromClient = new BufferedReader(
						new InputStreamReader(connectionSocket.getInputStream()));
				clientSentence = inFromClient.readLine();
				//System.out.println("Received: " + clientSentence);
			}
		} finally {
			welcomeSocket.close();
		}
	}
	
	public static void main(String[] args) throws IOException {
		start();
	}
	

}
