package misc;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.SortedMap;

import javax.crypto.NoSuchPaddingException;

import sync.FileSynchronizer;

public class Misc {
	static MyTreeMap<Integer, Integer> plainStartToPageID = new MyTreeMap<Integer, Integer>();
//	public static void main(String[] args) throws IOException {
//		File toto = new File("toto.txt");
//		plainStartToBlockID.put(0, 1);
//		plainStartToBlockID.put(1024, 2);
//		plainStartToBlockID.put(2040, 3);
////		Entry<Integer, Integer>[] l = (Entry<Integer, Integer>[]) plainStartToBlockID
////				.entrySet().toArray(new Entry[0]);
////		for(int i=0;i<l.length;i++){
////			System.out.println(l[i].getKey()+":"+l[i].getValue());
////		}
//		
////		System.out.println(getBlockFreeSpaceFromPlainPosition(2500));
//		System.out.println((int) toto.length());
//	}
//	public static int getBlockFreeSpaceFromPlainPosition(int plainPos){
//		return 1024-(plainStartToBlockID.ceilingKey(plainPos)-plainStartToBlockID.floorKey(plainPos));
//	}
	public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
//		for(int i=0;i<10;i++){
//			System.out.print(i);
//		}
//		for(char i='a';i<='z';i++){
//			System.out.print(i);
//		}
//		for(char i='A';i<='Z';i++){
//			System.out.print(i);
//		}
//			System.out.print(' ');
		
		
		FileSynchronizer fs = new FileSynchronizer(new File("plain.txt"),
				new File("encrypted.txt") ,new File("map.txt") , new String("youssef"),128);

		for(int i=0; i<10;i++){
			plainStartToPageID.put(i, i);
		}
		
		shiftStartingAt2(5, 3);
		System.out.println(plainStartToPageID);
//		try {
//			Field f=tree.getClass().getDeclaredField("entrySet");
//			List<Class<?>> classes= Arrays.asList(tree.getClass().getDeclaredClasses());
//			Class <?> c =classes.get(16);
//			c.newInstance();
//			System.out.println(classes);
//			//f.get(obj)
//		} catch (NoSuchFieldException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (SecurityException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (InstantiationException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IllegalAccessException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//		Arrays.asList(tree.getClass().getDeclaredFields()).forEach(e->System.out.println(e));
	}
	
	public static void shiftStartingAt(int pos, int dist) {// check compute
		plainStartToPageID
				.entrySet()
				.forEach(
						entry -> {
							MyTreeMap.Entry<Integer, Integer> e = (MyTreeMap.Entry<Integer, Integer>) entry;
							int k = e.getKey();
							if (k >= pos) {
								e.setKey(k + dist);
							}
						});
	}
	
	public static void shiftStartingAt2(int pos, int dist) {// check compute
	
		 SortedMap<Integer, Integer> tail = plainStartToPageID.tailMap(pos,
		 false);
		 SortedMap<Integer, Integer> head = plainStartToPageID
		 .headMap(pos, true);
		 // System.out.println("efm: Shifitng: head:"+head+" tail:"+tail);
		 plainStartToPageID = new MyTreeMap<Integer, Integer>(head);
		 // tail.forEach((k,v)->plainStartToPageID.remove(k));
		 tail.forEach((k, v) -> plainStartToPageID.put(k + dist, v));
	}
	
}
