package sync;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import misc.MyTreeMap;

import org.apache.commons.lang3.ArrayUtils;

import fileEncryptor.EncryptedFileManager;
import fileEncryptor.Page;
import fileEncryptor.SecureRandomAccessFile;

public class FileSynchronizer {
	private final File plainFile;
	private final File encryptedFile;
	private final File mapFile;
	
	private String password;
	
	private byte[] plainData;
	
	private RandomAccessFile plainRaf;
	private EncryptedFileManager efm;
	private MyTreeMap<Integer, Page> plainStartToPageID;
	
	
	
	// NB file and encrypted File always synchronized
	public FileSynchronizer(File plainFile, File encryptedFile, File mapFile, String password, int blockPerPage)
			throws IOException {
		this.encryptedFile = encryptedFile;
		this.plainFile = plainFile;
		this.mapFile=mapFile;
		this.password=password;
		init();
	}
	
//	/**
//	 * @param newData immutable array
//	 */
//	public void setNewPlain(byte[] newData){
//		try {
//			plainFile.delete();
//			plainFile.createNewFile();
//			plainRaf.write(newData);
//			plainData=newData;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	public void init() throws IOException{
		plainRaf = new RandomAccessFile(plainFile, "rw");
		////System.out.println("fs: plainLength: "+plainFile.length());
		plainData = new byte[(int) plainFile.length()];
		plainRaf.read(plainData);//load all
		SecureRandomAccessFile sraf;
		try {
			sraf = new SecureRandomAccessFile(encryptedFile, "rw", 2048,
					"AES/CTR/NoPadding", password, 16, 0);
			efm = new EncryptedFileManager(sraf,mapFile);
			plainStartToPageID=efm.getMap();
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO compare hashes to chekc synchronisation and decrypte every thing
		// if needed (to update local copy)
	}

	public long addAtPos(byte[] data, int pos) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException,
			InvalidAlgorithmParameterException, IOException {
		long start=System.nanoTime();
		Entry<Integer, Page> pageEntry = plainStartToPageID
				.floorEntry(pos);
		////System.out.println("fs: "+plainStartToPageID);
		int pageStart = pageEntry.getKey();
		int usedSpace = efm.getPageUsedSpaceFromPlainPosition(pos);
		////System.out.println("fs: PAGE_DIZE"+(efm.PAGE_SIZE )+" usedSpace: "+( usedSpace) +" dataLength"+ data.length);
		if (efm.PAGE_SIZE - usedSpace > data.length) {
			// plainStartToPageID.tailMap(pos).forEach((k,v)->k=k+data.length);//shift
			// all keys after pos
			////System.out.println("mix in one");
			byte[] newData = new byte[usedSpace + data.length];
			System.out.println("fs: pos: "+pos+" pageStart: "+pageStart+" plainLength: "+plainData.length+" pageUsedSpace: "+usedSpace);
			System.arraycopy(plainData, pageStart, newData, 0, pos - pageStart);
			System.arraycopy(data, 0, newData, pos - pageStart, data.length);
			System.arraycopy(plainData, pos, newData, pos
					- pageStart + data.length, usedSpace - (pos - pageStart));
			////System.out.println("fs: newData "+new String(newData)+" len="+newData.length);
			////System.out.println("fs: before write: "+plainStartToPageID);
			efm.writeInPage(Arrays.copyOf(newData, Math.min(efm.PAGE_SIZE,newData.length)), pageStart,
					pageEntry.getValue().id);//FIXME be carfull overflow
			efm.shiftStartingAt(pos, data.length);
			
		} else {
			
			Entry<Integer, Page> nextPageEntry = plainStartToPageID
					.higherEntry(pos);
			boolean mixWithNext;
			int sizeOfNextPage=0;
			int nextPageStart=0;
			if(nextPageEntry==null){//if no next page
				mixWithNext=false;
			}else{
				nextPageStart = nextPageEntry.getKey();
				sizeOfNextPage = efm
						.getPageUsedSpaceFromPlainPosition(nextPageStart);
				mixWithNext = usedSpace + sizeOfNextPage + data.length < 2 * efm.PAGE_SIZE;
			}
			int pageID2;
			byte[] newData;
			if (mixWithNext) {
				// mix pages
				////System.out.println("fs: mix with next page");
				newData = new byte[usedSpace + sizeOfNextPage + data.length];
				pageID2 = nextPageEntry.getValue().id;
			} else {
				// need new page
				newData = new byte[usedSpace + data.length];
				pageID2 = efm.getNextFreePageID();
			}
			System.arraycopy(plainData, pageStart, newData, 0, pos - pageStart);
			//////System.out.println(pageStart+"-"+0+"-"+(pos-pageStart));
			System.arraycopy(data, 0, newData, pos - pageStart, data.length);
			//////System.out.println(0+"-"+(pos-pageStart)+"-"+"data.length");
			
			////System.out.println("fs compare:"+(pos - pageStart+data.length+usedSpace - (pos - pageStart))+" "+(usedSpace+data.length));
			////System.out.println("fs: pos:"+pos+" newDataPos:"+(pos- pageStart + data.length)+" size: "+(pageStart+usedSpace-pos));
			////System.out.println("leftToCopy: "+(usedSpace-(pos-pageStart)));
			////System.out.println("usedSpace: "+usedSpace+" pos:"+pos+" pageStart: "+pageStart);
			
			System.arraycopy(plainData, pos, newData, pos
					- pageStart + data.length, usedSpace-(pos-pageStart));//FIXME
			if (mixWithNext) {
				System.arraycopy(plainData, nextPageStart, newData, usedSpace
						+ data.length, sizeOfNextPage);
				//write first page
				efm.writeInPage(Arrays.copyOf(newData, efm.PAGE_SIZE), pageStart,
						pageEntry.getValue().id);
				//write second page
				efm.writeInPage(
						Arrays.copyOfRange(newData, efm.PAGE_SIZE, newData.length),
						pageStart + efm.PAGE_SIZE, pageID2);
				//shift all
				efm.shiftStartingAt(nextPageStart, data.length);
			}else{
				//write first page
				efm.writeInPage(Arrays.copyOf(newData, efm.PAGE_SIZE), pageStart,
						pageEntry.getValue().id);
				//shift all
				efm.shiftStartingAt(pos, data.length);
				//write second page
				efm.writeInPage(
						Arrays.copyOfRange(newData, efm.PAGE_SIZE, newData.length),
						pageStart + efm.PAGE_SIZE, pageID2);
			}
			//efm.shiftStartingAt(pos, data.length);
			
		}
		//update plain data:
		byte[] newPlainData=new byte[plainData.length+data.length];
		System.arraycopy(plainData, 0, newPlainData, 0, pos);
		System.arraycopy(data, 0, newPlainData, pos, data.length);
		System.arraycopy(plainData, pos, newPlainData, pos+data.length, plainData.length-pos);
		plainData=newPlainData;		
		////System.out.println("updated plainData: "+new String(Arrays.copyOf(plainData, 3200)));
		
		plainRaf.seek(pos);
		plainRaf.write(plainData, pos,plainData.length-pos);
		
		//TODO update plainData In File
		//TODO update map in file
		////System.out.println("fs: map: "+plainStartToPageID);
		efm.updateFileMapInFile();//FIXME
		return System.nanoTime()-start;
		
		
		
	}
	
	public long removeFromPos(int pos, int size) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException, ShortBufferException{
		long start=System.nanoTime();
		long start2;
		start2=System.nanoTime();
		Entry<Integer,Page> beginPageEntry=plainStartToPageID.floorEntry(pos);
		Entry<Integer,Page> endPageEntry=plainStartToPageID.floorEntry(pos+size);
		int usedSpaceInEndPage=efm.getPageUsedSpaceFromPlainPosition(endPageEntry.getKey());
		//System.out.println("remove: init: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
//		SortedSet<Integer> keySet= plainStartToPageID.navigableKeySet();
//		//System.out.println("remove: keySet: "+(System.nanoTime()-start2));
//		start2=System.nanoTime();
//		SortedSet<Integer> inBetweenKeys=keySet.subSet(beginPageEntry.getKey(), endPageEntry.getKey()+1);
		SortedMap<Integer, Page> inBetweenEntries = plainStartToPageID.subMap(beginPageEntry.getKey(), endPageEntry.getKey()+1);
		//System.out.println("remove: inBetweenEntries: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
		//Set<Integer> inBetweenKeys= inBetweenEntries.keySet();
		//System.out.println("remove: inBetweenKeys: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
		int sizeOfInBetweenPages=inBetweenEntries.lastKey()-inBetweenEntries.firstKey()+usedSpaceInEndPage;
		//int extraPage=(sizeOfInBetweenPages-size)/efm.PAGE_SIZE;//should never be greater than 1
		int sizeLeftModPagesize=(sizeOfInBetweenPages-size)%efm.PAGE_SIZE;
		//System.out.println("remove: sizes: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
//		byte[] remainingBeginPage=efm.getPlainFromTo(beginPageEntry.getKey(), pos);
//		byte[] remainingEndPage=efm.getPlainFromTo(pos+size, endPageEntry.getKey()+usedSpaceInEndPage);
//		start2=System.nanoTime();
//		byte[] remainingData=ArrayUtils.addAll(remainingBeginPage, remainingEndPage);
		byte[] remainingData=new byte[sizeOfInBetweenPages-size];
		//System.out.println("fs: pos:"+pos+" size:"+size);
		////System.out.println("fs: plainData.length:"+plainData.length+" beginPageEntry.getKey():"+beginPageEntry.getKey()+" remainingData.length:"+remainingData.length+" sizeToCOpy:"+(pos-beginPageEntry.getKey()));
		System.arraycopy(plainData, beginPageEntry.getKey(), remainingData, 0, pos-beginPageEntry.getKey());
		////System.out.println("fs: plainData.length:"+plainData.length+" start2:"+(beginPageEntry.getKey()+pos+size)+" remainingData.length:"+remainingData.length+" bufferStart2:"+(pos-beginPageEntry.getKey())+" sizeToCopy2:"+ ((endPageEntry.getKey()+usedSpaceInEndPage)-(pos+size)));
		System.arraycopy(plainData, pos+size, remainingData, pos-beginPageEntry.getKey(), (endPageEntry.getKey()+usedSpaceInEndPage)-(pos+size));
		boolean mixed=false;
		//System.out.println("remove: remainingData: "+(System.nanoTime()-start2));
		//check mix what's left with previous
		start2=System.nanoTime();
		if(beginPageEntry.getKey()!=0){//first key always 0
			//if not first page
			Entry<Integer,Page> previousPageEntry=plainStartToPageID.floorEntry(beginPageEntry.getKey()-1);
			if(beginPageEntry.getKey()-previousPageEntry.getKey()+sizeLeftModPagesize<=efm.PAGE_SIZE ){
				// mix with previous
				////System.out.println("fs remove mix with previous");
				mixed=true;
				//if this is last page:
				if(endPageEntry.getKey().equals(plainStartToPageID.lastKey())){
					efm.setLastPageSize((sizeLeftModPagesize+previousPageEntry.getKey())%efm.PAGE_SIZE);
				}
				
				//if I can mix both pages
				byte[] previousPageContent=efm.getPlainFromPageContainingPos(previousPageEntry.getKey());
				byte[] content=ArrayUtils.addAll(previousPageContent, remainingData);
				int pageIds[];
				if(content.length>2*efm.PAGE_SIZE){
					pageIds=new int[]{previousPageEntry.getValue().id,efm.getNextFreePageID(),endPageEntry.getValue().id};
				}else{
					pageIds=new int[]{previousPageEntry.getValue().id,endPageEntry.getValue().id};
				}
				//remove all in between pages and upadte keys
				inBetweenEntries.clear();
				//inBetweenKeys.forEach(k->plainStartToPageID.remove(k));
 				efm.shiftStartingAt(pos+size, -size);
				for(int i=0;i*efm.PAGE_SIZE<content.length;i++){
					byte[] b=Arrays.copyOfRange(content, i*efm.PAGE_SIZE, i*efm.PAGE_SIZE+Math.min(efm.PAGE_SIZE, content.length-(i*efm.PAGE_SIZE)));
					efm.writeInPage(b, previousPageEntry.getKey()+(i*efm.PAGE_SIZE), pageIds[i]);
				}
			}
		}
		//System.out.println("remove: mixWithPrevious: "+(System.nanoTime()-start2));
		//else check mix what's left with next
		start2=System.nanoTime();
		if(!mixed && endPageEntry.getKey()<plainStartToPageID.lastKey()){
			Entry<Integer,Page> nextPageEntry=plainStartToPageID.higherEntry(endPageEntry.getKey());
			if(efm.getPageUsedSpaceFromPlainPosition(nextPageEntry.getKey())+sizeLeftModPagesize<=efm.PAGE_SIZE){
				////System.out.println("fs remove mix with next");
				mixed=true;
				//if next is last page:
				if(nextPageEntry.getKey().equals(plainStartToPageID.lastKey())){
					efm.setLastPageSize((sizeLeftModPagesize+nextPageEntry.getKey())%efm.PAGE_SIZE);
				}
				
				//if I can mix both pages
				byte[] nextPageContent=efm.getPlainFromPageContainingPos(nextPageEntry.getKey());
//				byte[] remainingBeginPage=efm.getPlainFromTo(beginPageEntry.getKey(), pos);
//				byte[] remainingLastPage=efm.getPlainFromTo(pos+size, endPageEntry.getKey()+usedSpaceInEndPage);
//				byte[] remainingData=ArrayUtils.addAll(remainingBeginPage, remainingLastPage);
				byte[] content=ArrayUtils.addAll(remainingData,nextPageContent);
				int pageIds[];
				if(content.length>2*efm.PAGE_SIZE){
					pageIds=new int[]{beginPageEntry.getValue().id,efm.getNextFreePageID(),nextPageEntry.getValue().id};
				}else{
					pageIds=new int[]{beginPageEntry.getValue().id,nextPageEntry.getValue().id};
				}
				//remove all in between pages and upadte keys
				inBetweenEntries.clear();
				//inBetweenKeys.forEach(k->plainStartToPageID.remove(k));
				plainStartToPageID.remove(nextPageEntry.getKey());
 				efm.shiftStartingAt(pos+size, -size);
				for(int i=0;i*efm.PAGE_SIZE<content.length;i++){
					efm.writeInPage(Arrays.copyOfRange(content, i*efm.PAGE_SIZE, i*efm.PAGE_SIZE+Math.min(efm.PAGE_SIZE, content.length-(i*efm.PAGE_SIZE))), beginPageEntry.getKey()+(i*efm.PAGE_SIZE), pageIds[i]);
				}
			}
			
		}
		//System.out.println("remove: mixWithNext: "+(System.nanoTime()-start2));
		//if can't mix
		start2=System.nanoTime();
		if(!mixed){
			long start3;
			start3=System.nanoTime();
			////System.out.println("no mix");
			//if this is last page:
			if(endPageEntry.getKey().equals(plainStartToPageID.lastKey())){
				efm.setLastPageSize((sizeLeftModPagesize));
			}
			//System.out.println("remove3: setLastPageSize: "+(System.nanoTime()-start3));
			start3=System.nanoTime();
			int pageIds[]=new int[]{beginPageEntry.getValue().id,endPageEntry.getValue().id};
			//System.out.println("remove3: pageIds: "+(System.nanoTime()-start3));
			start3=System.nanoTime();
			//inBetweenEntries.clear();
			inBetweenEntries.clear();
			//System.out.println("remove3: clear: "+(System.nanoTime()-start3));
			start3=System.nanoTime();
			efm.shiftStartingAt(pos+size, -size);
			//System.out.println("remove3: shiftStartingAt: "+(System.nanoTime()-start3));
			start3=System.nanoTime();
			for(int i=0;i*efm.PAGE_SIZE<remainingData.length;i++){
				////System.out.println("fs: remainingsize: "+remainingData.length+" beginPageID: "+beginPageEntry.getValue()+" endPageID: "+endPageEntry.getValue());
				efm.writeInPage(Arrays.copyOfRange(remainingData, i*efm.PAGE_SIZE, i*efm.PAGE_SIZE+Math.min(efm.PAGE_SIZE, remainingData.length-(i*efm.PAGE_SIZE))), beginPageEntry.getKey()+(i*efm.PAGE_SIZE), pageIds[i]);
			}
			//System.out.println("remove3: writeInPages: "+(System.nanoTime()-start3));
		}
		//System.out.println("remove: !mixed: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
		byte[] newPlainData=new byte[plainData.length-size];
		System.arraycopy(plainData, 0, newPlainData, 0, pos);
		System.arraycopy(plainData, pos+size, newPlainData, pos, plainData.length-pos-size);
		plainData=newPlainData;
		////System.out.println("updated plainData: "+new String(Arrays.copyOf(plainData, 3200)));
		//specialkeys when file bcomes empty check firt key exists
		if(plainStartToPageID.isEmpty()){
			plainStartToPageID.put(0, new Page(0, BigInteger.valueOf(0)));
		}
		//System.out.println("remove: newPlainData: "+(System.nanoTime()-start2));
		start2=System.nanoTime();
		efm.updateFileMapInFile();//FIXME
		//System.out.println("remove: updateMapInfile: "+(System.nanoTime()-start2));
		long end=System.nanoTime();
		plainRaf.seek(pos);
		plainRaf.write(plainData, pos,plainData.length-pos);
		plainRaf.setLength(plainData.length);
		return end-start;
	}
	
	public byte[] getPlainFromTo(int begin, int end) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, ShortBufferException{
		return efm.getPlainFromTo(begin, end);
	}

	public void updateAll() {
		
		try {
			efm.close();
			encryptedFile.delete();
			encryptedFile.createNewFile();
			init();
			int nbrOfPages = (int) Math
					.ceil(plainData.length / (double) efm.PAGE_SIZE);
			for (int i = 0; i < nbrOfPages; i++) {
				int freeId = efm.getNextFreePageID();
				int start = i * efm.PAGE_SIZE;
				efm.writeInPage(
						Arrays.copyOfRange(plainData, start, start + efm.PAGE_SIZE),
						start, freeId);
			}
			efm.updateFileMapInFile();
		} catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
			System.err.println("update failed");
			e.printStackTrace();
		}
	}

	public EncryptedFileManager getEfm() {
		return efm;
	}

	public String getPlainString() {
		return new String(plainData);
	}
}
