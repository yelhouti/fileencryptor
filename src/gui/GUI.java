package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import server.DummyServer;
import sun.rmi.runtime.NewThreadAction;
import sync.FileSynchronizer;

public class GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2168438510319356631L;
	
	boolean removeFiles=false;
	
	
	final int RANDOM_SIZE_IN_KB = 50000;
	JPanel pan = new JPanel();
	JPanel rightMenuPan = new JPanel();
	JPanel textPan = new JPanel();
	JPanel topPan = new JPanel();
	JPanel bottomPan = new JPanel();
	JButton encrypt = new JButton("encrypt");
	JButton random = new JButton("Random Text");
	JButton benchmark = new JButton("benchmark");
	JLabel passwordLabel = new JLabel("password");
	JPasswordField passwordField = new JPasswordField("youssef");

	String homeFolder=System.getProperty("user.home");
	File plainFile = new File("plain.txt");
	File encryptedFile = new File(homeFolder+"/Dropbox/encrypted.txt");
	File mapFile = new File(homeFolder+"/Dropbox/map.txt");
	File naiveFile = new File("naive.txt");

	FileSynchronizer fs;
	int blockPerPage = 10;
	
//	private Socket naiveSocket;
//	private DataOutputStream naiveSOS;

	RandomAccessFile naiveRAF = new RandomAccessFile(naiveFile, "rw");

	public ArrayList<long[]> results = new ArrayList<long[]>();

	MyPlainDocument document = new MyPlainDocument();
	long durations = 0;
	class MyPlainDocument extends PlainDocument {
		

		public MyPlainDocument() {
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -679834148114722363L;

		public void remove(int offs, int len)
				throws javax.swing.text.BadLocationException {
			try {
				// System.out.println("GUI remove offs="+offs+" len="+len);
				long t = fs.removeFromPos(offs, len);
				durations += t;
				super.remove(offs, len);
				cipher.setText(fs.getEfm().getMap().toString());
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | InvalidAlgorithmParameterException
					| IOException | ShortBufferException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void init(String value) throws BadLocationException {
			super.remove(0, this.getLength());
			super.insertString(0, value, null);
		}

		public void insertString(int offs, String str,
				javax.swing.text.AttributeSet a)
				throws javax.swing.text.BadLocationException {
			try {
				// System.out.println("GUI: insertString at offs: "+offs);
				long t = fs.addAtPos(str.getBytes(), offs);
				durations += t;
				// System.out.println("duration of smart encryption: "+t);
				super.insertString(offs, str, a);
				cipher.setText(fs.getEfm().getMap().toString());
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | InvalidAlgorithmParameterException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
	};

	JTextArea plain = new JTextArea();
	JScrollPane plainScroll = new JScrollPane(plain);
	JTextArea cipher = new JTextArea();
	JScrollPane cipherScroll = new JScrollPane(cipher);

	public GUI() throws IOException, BadLocationException {
		//runServer();
		if(removeFiles){
			plainFile.delete();
			encryptedFile.delete();
			mapFile.delete();
		}	
//		naiveSocket = new Socket("localhost", 6789);
//		naiveSOS=new DataOutputStream(naiveSocket.getOutputStream());
		fs = new FileSynchronizer(plainFile, encryptedFile, mapFile,
				new String(passwordField.getPassword()), blockPerPage);
		// System.out.println("password: "+new
		// String(passwordField.getPassword()));
		plain.setDocument(document);

		this.setContentPane(pan);
		pan.setLayout(new BorderLayout());
		pan.add(textPan, BorderLayout.CENTER);
		pan.add(rightMenuPan, BorderLayout.EAST);
		textPan.setLayout(new GridLayout(2, 1));
		topPan.setLayout(new BorderLayout());
		bottomPan.setLayout(new BorderLayout());
		// topPan.setLayout(new BorderLayout());
		// bottomPan.setLayout(new BorderLayout());
		textPan.add(topPan);
		textPan.add(bottomPan);

		topPan.add(plainScroll);
		bottomPan.add(cipherScroll);
		cipher.setEnabled(false);

		rightMenuPan.setLayout(new GridLayout(10, 1));
		rightMenuPan.add(encrypt);
		rightMenuPan.add(random);
		rightMenuPan.add(benchmark);
		rightMenuPan.add(passwordLabel);
		rightMenuPan.add(passwordField);

		plain.setLineWrap(true);
		plain.setWrapStyleWord(true);

		cipher.setLineWrap(true);
		cipher.setWrapStyleWord(true);

		this.setMinimumSize(new Dimension(1000, 1000));
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		((MyPlainDocument) plain.getDocument()).init(fs.getPlainString());
		cipher.setText(fs.getEfm().getMap().toString());
		random.addActionListener(e -> setRandomText(RANDOM_SIZE_IN_KB, true));
		encrypt.addActionListener(e -> naiveEncrypt());
		benchmark.addActionListener(e -> benchmark());
		// plain.getDocument().addDocumentListener(dl);
	}

	private void runServer() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					DummyServer.start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

	public long naiveEncrypt() {
		long start = System.nanoTime();
		try {
			final String DIGEST = "SHA-256";
			final byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			final IvParameterSpec ivspec;
			Cipher cipherEnc;
			MessageDigest digest = MessageDigest.getInstance(DIGEST);
			int KEY_SIZE = 16;
			String ENCRYPTION_ALGORITHM = "AES/CTR/NoPadding";
			String password = "youssef";
			byte[] key = Arrays.copyOf(
					digest.digest((password).getBytes("UTF-8")), KEY_SIZE);
			ivspec = new IvParameterSpec(iv);
			cipherEnc = Cipher.getInstance(ENCRYPTION_ALGORITHM);
			SecretKeySpec keySpec = new SecretKeySpec(key,
					ENCRYPTION_ALGORITHM.split("/")[0]);
			cipherEnc.init(Cipher.ENCRYPT_MODE, keySpec, ivspec);
			byte[] data = cipherEnc.doFinal(plain.getText().getBytes());
//			long start2 =System.nanoTime();
			naiveRAF.write(data, 0, data.length);
			naiveRAF.setLength(data.length);
//			System.out.println("naiveRafWrite: "+(System.nanoTime()-start2));
//			start2 =System.nanoTime();
			//naiveSOS.write(data);
//			System.out.println("naiveSOSWrite: "+(System.nanoTime()-start2));
//			System.out.println("naiveDataLength: "+data.length);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException
				| NoSuchAlgorithmException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (System.nanoTime() - start);
	}

	public void setRandomText(int sizeInKB, boolean confirmation) {
		int confirmed = JOptionPane.YES_OPTION;
		if (plain.getText().length() != 0 && confirmation) {
			confirmed = JOptionPane.showConfirmDialog(null,
					"Modifie current Text", "Generate new text?",
					JOptionPane.YES_NO_OPTION);
		}
		if (confirmed == JOptionPane.YES_OPTION) {
			// System.out.println("Setting random text");
			// plainFile.delete();
			// System.out.println("GUI: delete");
			// plain.setText("");

//			 StringBuilder sb= new StringBuilder();
//			 for(int i=0;i<RANDOM_SIZE_IN_KB/4;i++){
//			 sb.append(StringUtils.join(new LoremIpsum4J()
//			 .getBytes(4096),"\n"));
//			 }
			String newText = org.apache.commons.lang3.RandomStringUtils
					.random(1000 * sizeInKB,
							" 0123456789 abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ ");// sb.toString();
			// try {
			// plainFile.createNewFile();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			plain.setText(newText);
			// System.out.println("gui newTextlength: "+newText.length());
		}

	}

	public long modify(int nbrOfRemovals, int nbrOfInserations)
			throws BadLocationException {
		MyPlainDocument doc = (MyPlainDocument) plain.getDocument();
		durations=0;
		for (int i = 0; i < nbrOfRemovals; i++) {
			//long start=System.nanoTime();
			plain.getDocument()
					.remove(new Random().nextInt(doc.getLength()), 1);
			//System.out.println("one removal duration: "+(System.nanoTime()-start));
		}
		for (int i = 0; i < nbrOfInserations; i++) {
			plain.getDocument().insertString(
					new Random().nextInt(doc.getLength()), "X", null);
		}

		return durations;
	}

	public void caseTest(int nbrOfInserations, int nbrOfRemovals,
			int blockPerPage, int fileSizeInKB) throws BadLocationException {
		plainFile.delete();
		encryptedFile.delete();
		mapFile.delete();
		try {
			fs = new FileSynchronizer(plainFile, encryptedFile, mapFile,
					new String(passwordField.getPassword()), blockPerPage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		plain.setDocument(new MyPlainDocument());
		setRandomText(fileSizeInKB, false);
		long totalDuration = modify(nbrOfRemovals, nbrOfInserations);
		plain.setDocument(new MyPlainDocument());
		setRandomText(fileSizeInKB, false);
		long naiveDuration = naiveEncrypt();
		long[] res= new long[] { nbrOfInserations, nbrOfRemovals, blockPerPage,
				fileSizeInKB, totalDuration, naiveDuration };
		System.out.println(Arrays.toString(res));
		results.add(res);
	}

	public void benchmark() {
		// stupid values NEED to be changed
//		int[] blockPerPages = new int[] { 16,62,128,256 }; // {1,4,16,62,128,256,1024,2048,4096};
//		int[] filesSizesInKB = new int[] {100};
//		int[] removals = new int[] { 0 };
//		int[] insertions = new int[] { 1 };
		int[] blockPerPages = new int[] { 256 }; // {1,4,16,62,128,256,1024,2048,4096};
		int[] filesSizesInKB = new int[] {1024*128};
		int[] removals = new int[] {1};
		int[] insertions = new int[] { 1};
		System.out
		.println("[nbrOfInserations, nbrOfRemovals, blockPerPage, fileSizeInKB, totalDuration, naiveDuration]");
		try {
			for (int i : insertions) {
				for (int j : removals) {
					for (int k : blockPerPages) {
						for (int l : filesSizesInKB) {
							caseTest(i, j, k, l);
						}
					}
				}
			}
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("results recap:");
		System.out
				.println("[nbrOfInserations, nbrOfRemovals, blockPerPage, fileSizeInKB, totalDuration, naiveDuration]");
		results.forEach(e -> {
			System.out.println(Arrays.toString(e));
			cipher.setText(cipher.getText() + "\n " + Arrays.toString(e));
		});
	}

	// DocumentListener dl = new DocumentListener() {
	//
	// @Override
	// public void removeUpdate(DocumentEvent e) {
	// cipher.setText(e.getType().toString());
	// }
	//
	// @Override
	// public void insertUpdate(DocumentEvent e) {
	// cipher.setText(e.getType().toString());
	// }
	//
	// @Override
	// public void changedUpdate(DocumentEvent e) {
	// cipher.setText(e.getType().toString());
	// }
	// };

	public static void main(String[] args) throws IOException,
			BadLocationException {
		new GUI();
	}
}
