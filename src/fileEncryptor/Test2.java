package fileEncryptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Test2 {
	public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		File toto = new File("toto.txt");
		File etoto = new File("etoto.txt");
		
		RandomAccessFile plainRaf= new RandomAccessFile(toto, "rw");
		byte[] plain=new byte[(int) toto.length()];
		plainRaf.readFully(plain);
		plainRaf.close();
		SecureRandomAccessFile sraf = new SecureRandomAccessFile(etoto, "rw", 16, "AES/CTR/NoPadding", "youssef", 16, 0);
		int blockSize=16;//sraf.getBLOCK_SIZE();
		int nbrOfBlocks=(int) Math.ceil(toto.length()/(double)blockSize);
		for (int i=0; i<nbrOfBlocks; i++){
//			sraf.writeInBlock(Arrays.copyOfRange(plain, i*blockSize,(1+i)*blockSize), i);
		}
		
		StringBuilder sb= new StringBuilder();
		for (int i=0; i<nbrOfBlocks; i++){
//			sb.append(new String(sraf.readBlocks(i,i+1)));
		}
		System.out.println(sb);
		
		System.out.println();
		if(sb.substring(0,plain.length).equals(new String(plain))){
			System.out.println("plain is equal to decrypted");
		}else{
			System.out.println("error: plain mismatch decrypted");
		}
	}
}
