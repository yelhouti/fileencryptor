package fileEncryptor;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import misc.MyTreeMap;

public class EncryptedFileManager {
	private MyTreeMap<Integer, Page> map;
	private SecureRandomAccessFile sraf;
	private ArrayList<Integer> mapPagesIDs;
	private int lastPageSize;
	private File fileMap;
	private RandomAccessFile mapRaf;
	private BigInteger lastKey=BigInteger.valueOf(0);
//	private Socket clientSocket;
//	private DataOutputStream socketos;

	public int getLastPageSize() {
		return lastPageSize;
	}

	public void setLastPageSize(int lastPageSize) {
		this.lastPageSize = lastPageSize;
	}

	public final int PAGE_SIZE;
	MessageDigest digest;
	final int PADDING;
	private final int FIRST_MAP_PAGE_ID = 0;

	/*
	 * MAP contains entries (start plaintext byte, BLOCK ID)+size of latest
	 * page(int)+next Map Block ID (int) (MAP SIZE = BLOCK SIZE) Encrypted DATA
	 * starts at BLOCK_ID 1
	 */
	public EncryptedFileManager(SecureRandomAccessFile sraf, File fileMap) throws FileNotFoundException {
		mapRaf= new RandomAccessFile(fileMap, "rw");
		this.sraf = sraf;
		this.fileMap=fileMap;
		// try {
		// digest = MessageDigest.getInstance("SHA-256");
		// } catch (NoSuchAlgorithmException e) {
		// // TODO Auto-generated catch page
		// e.printStackTrace();
		// }
		// PADDING = digest.getDigestLength();�
		PAGE_SIZE = sraf.AES_BLOCK_SIZE * sraf.BLOCK_PER_PAGE;// in bytes
		PADDING = 0;// TODO ADD HASH and change PADDING
		populateMap();
//		try {
//			clientSocket = new Socket("localhost", 6789);
//			socketos=new DataOutputStream(clientSocket.getOutputStream());
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public void updateFileMapInFile() {
		Iterator<Entry<Integer, Page>> it = map
				.entrySet().iterator();
		ByteBuffer bf = ByteBuffer.allocate((map.size()*(2*Integer.BYTES+sraf.AES_BLOCK_SIZE)+Integer.BYTES));
		try {
			Entry<Integer, Page> entry = null;
			ByteBuffer keybf= ByteBuffer.allocate(sraf.AES_BLOCK_SIZE);
			while (it.hasNext()) {
				entry = it.next();
				bf.putInt(entry.getKey());
				bf.putInt(entry.getValue().id);
				keybf.position(sraf.AES_BLOCK_SIZE-entry.getValue().key.toByteArray().length);
				keybf.put(entry.getValue().key.toByteArray());
				bf.put(keybf.array());
			}
			bf.putInt(lastPageSize);
			
			mapRaf.seek(0);
			System.out.println("efm: wrttingBuffer: "+Arrays.toString(bf.array())+" lastPageSize: "+lastPageSize);
			mapRaf.write(bf.array());
			System.out.println("efm: block key size:"+keybf.capacity()+", AES_BLOCK_SIZE:"+sraf.AES_BLOCK_SIZE);
			System.out.println("efm: last written key: "+Arrays.toString(keybf.array()));
			//			socketos.write(bf.array());
		} catch (IOException e) {
			// TODO Auto-generated catch page
			e.printStackTrace();// shoulf not happen
		}
		
//		System.out.println("efm: wrtiten map: "+plainStartToPageID);
//		System.out.println("efm: updatefileMap: lastPageSize: "+lastPageSize);
	}

	private void populateMap() {// Should be in constructor
		int plainStart=0;
		int pageID;
		map = new MyTreeMap<Integer, Page>();
		byte[] keyBuffer=new byte[sraf.AES_BLOCK_SIZE];
		try {	
			mapRaf.seek(0);
			System.out.println("efm: populatingMap: mapLength"+mapRaf.length());
			do {
				plainStart = mapRaf.readInt();
				pageID = mapRaf.readInt();
				mapRaf.read(keyBuffer);
				BigInteger key= new BigInteger(keyBuffer);
				lastKey=lastKey.max(key);
				map.put(plainStart, new Page(pageID,key));
				System.out.println("efm: populate: add to map: "+plainStart+": "+new Page(pageID,new BigInteger(keyBuffer))+" keybuffer: "+Arrays.toString(keyBuffer));
			} while (true);
		} catch (IOException e) {
			lastPageSize=plainStart;
			System.out.println("efm: lastpagesize: "+lastPageSize);
			// reached and of file
			if(map.isEmpty()){
				map.put(0, new Page(0,BigInteger.valueOf(0)));
			}
		}
//		System.out.println("efm: initial map: "+plainStartToPageID);
//		System.out.println("efm: populate: lastPageSize: "+lastPageSize);
	}

	// public List<Integer> getMapPagesIDs() {
	// List<Integer> mapPageIDs = new ArrayList<Integer>();
	// int nextMapPageID = mapPagesIDs.get(0);
	// try {
	// do {
	// sraf.seek(PADDING + (1 + nextMapPageID) * PAGE_SIZE
	// - Integer.SIZE);
	// sraf.readPlainInt();// readLatsBlockSize
	// nextMapPageID = sraf.readPlainInt();
	// mapPageIDs.add(nextMapPageID);
	// } while (nextMapPageID != 0);
	// } catch (IOException e) {
	// //add map 0 if reached end
	// mapPageIDs.add(0);
	// }
	// return mapPageIDs;
	// }

	public int getNextFreePageID() {
		/*
		 * TODO REMOVE THIS FROM HERE AND DO THE CALCULATION ONES in the
		 * BIGINNING do not forget to keep it updated after writes
		 */
		int i = 0;
		Iterator<Page> it1=map.values().iterator();
		while(it1.hasNext()){
			if(i==it1.next().id){
				i++;
			}else{
				return i;
			}
		}
		return i;
	}
	/*
	 * Use carefully in order to not oveflow when writing in holes
	 */
	public void writeInPage(byte[] buffer, int plainStart, int pageID)
			throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException,
			IOException {
		
		
//		socketos.write(buffer);
		int pid = pageID;
		for (int i = 0; i < buffer.length; i += PAGE_SIZE) {
			lastKey=lastKey.add(BigInteger.ONE);
			sraf.writeInPage(buffer, pageID, lastKey);
			map.put(plainStart + i,  new Page(pid,lastKey));
			pid++;
		}
		// System.out.println("newLength if end="+(plainStart+buffer.length)+" lastLength"+
		// (plainStartToPageID.lastKey()+lastPageSize));
		if (plainStart + buffer.length > map.lastKey()
				+ lastPageSize) {
			lastPageSize = (plainStart + buffer.length - map
					.lastKey()) % PAGE_SIZE;
		}
		// System.out.println("efm: end of writing: map: "+plainStartToPageID);
	}

	public int getPageFreeSpaceFromPlainPosition(int plainPos) {
		return PAGE_SIZE - getPageUsedSpaceFromPlainPosition(plainPos);
	}

	public int getPageUsedSpaceFromPlainPosition(int plainPos) {
		// //System.out.println("em: lastpageSize="+lastPageSize);
		try {
			// //System.out.println("efm: next entry: "+plainStartToPageID.higherEntry(plainPos));
			return (map.higherKey(plainPos) - map
					.floorKey(plainPos));
		} catch (NullPointerException e) {
			// System.out.println("efm: lastpagesize: "+lastPageSize);
			return lastPageSize;
		}
	}

//	public byte[] getPlainFromPage(int pageID) throws InvalidKeyException,
//			IllegalBlockSizeException, BadPaddingException,
//			NoSuchAlgorithmException, NoSuchPaddingException,
//			InvalidAlgorithmParameterException, IOException {
//		return sraf.readPage(pageID,map.get(key));
//	}
//
	public byte[] getPlainFromPageContainingPos(int pos)
			throws InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IOException, ShortBufferException {
		Page page=map.floorEntry(pos).getValue();
		byte[] buf=new byte[PAGE_SIZE];
		sraf.readPage(page.id, page.key, buf, 0);
		
		return Arrays.copyOfRange(buf, 0,
				getPageUsedSpaceFromPlainPosition(pos));
	}

	public MyTreeMap<Integer, Page> getMap() {
		return map;
	}

	public byte[] getPlainFromTo(int begin, int end)
			throws InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IOException, ShortBufferException {
		// System.out.println("efm: begin="+begin+" end="+end);
		byte[] res = new byte[end - begin];

		ByteBuffer buffer = ByteBuffer.allocate(end - begin);
		// special case first because not always in begining of a page:
		// int sizeTokenFromFirstPage=;
		Integer higherKey = map.higherKey(begin);
		if (higherKey == null) {
			higherKey = Integer.MAX_VALUE;
		}

		byte[] fistPageNeededData = Arrays.copyOfRange(
				getPlainFromPageContainingPos(begin), begin
						- map.floorKey(begin),
				Math.min(end - map.floorKey(begin), higherKey));
		buffer.put(fistPageNeededData);
		begin += fistPageNeededData.length;
		while (begin < end) {
			byte[] b = getPlainFromPageContainingPos(begin);
			try {
				if (b.length == 0)
					Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				buffer.put(b);
			} catch (java.nio.BufferOverflowException e) {
				buffer.put(Arrays.copyOf(b, buffer.remaining()));
			}
			begin += b.length;
		}
		buffer.position(0);
		buffer.get(res);
		return res;

	}

	public void shiftStartingAt(int pos, int dist) {// check compute
		map
				.entrySet()
				.forEach(
						entry -> {
							MyTreeMap.Entry<Integer, Page> e = (MyTreeMap.Entry<Integer, Page>) entry;
							int k = e.getKey();
							if (k > pos) {
								e.setKey(k + dist);
							}
						});
		// SortedMap<Integer, Integer> tail = plainStartToPageID.tailMap(pos,
		// false);
		// SortedMap<Integer, Integer> head = plainStartToPageID
		// .headMap(pos, true);
		// // System.out.println("efm: Shifitng: head:"+head+" tail:"+tail);
		// plainStartToPageID = new MyTreeMap<Integer, Integer>(head);
		// // tail.forEach((k,v)->plainStartToPageID.remove(k));
		// tail.forEach((k, v) -> plainStartToPageID.put(k + dist, v));
	}

	public void close() throws IOException {
		sraf.close();
	}

	// FIXME remove me
	public SecureRandomAccessFile getSraf() {
		return sraf;
	}
}
