package fileEncryptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import sync.FileSynchronizer;

public class Main {
	public static void main(String[] args) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException, ShortBufferException {
		File toto = new File("toto.txt");
		File etoto = new File("etoto.txt");
		File mtoto = new File("mtoto.txt");
		FileSynchronizer fs= new FileSynchronizer(toto, etoto, mtoto,"youssef",128);
		fs.updateAll();
		fs.addAtPos("tototo".getBytes(), 0);
//		fs.addAtPos("titi".getBytes(), 1600);
//		fs.removeFromPos(1650, 5);
		System.out.println(fs.getEfm().getMap());
		System.out.println(new String(fs.getPlainFromTo(0, 10000)));
	}
}