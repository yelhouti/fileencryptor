package fileEncryptor;

import java.math.BigInteger;

public class Page {
	public int id;
	BigInteger key;
	public Page(int id, BigInteger key) {
		this.id = id;
		this.key = key;
	}
	
	@Override
	public String toString() {
		return "("+id+":"+key+")";
	}
	
}
