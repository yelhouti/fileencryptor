package fileEncryptor;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Test {
	private static final int BLOCK_SIZE = 16;
	private final static String ENCRYPTION_ALGORITHM="AES/CTR/NoPadding";
	private final static int KEY_SIZE=16;
	private static String password="youssef";
	private static final String DIGEST = "SHA-256";
	private final static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		
//		File toto = new File("toto.txt");
//		File etoto = new File("etoto.txt");
//		
//		RandomAccessFile plainRaf= new RandomAccessFile(toto, "rw");
//		byte[] plain=new byte[(int) toto.length()];
//		plainRaf.readFully(plain);
//		plainRaf.close();
//		System.out.println(new String(plain));
//		RandomAccessFile encRaf= new RandomAccessFile(etoto, "rw");
//		encRaf.write(aesEnc.doFinal(plain));
//		encRaf.seek(0);
//		
//		byte[] enc=new byte[(int) etoto.length()];
//		encRaf.readFully(enc);
//		System.out.println(new String(aesDec.doFinal(enc)));
//		encRaf.close();
		
		int blockID=0;
		Cipher aesEnc= getCipherForBlock(Cipher.ENCRYPT_MODE, blockID);
		byte[] enc=aesEnc.doFinal("toto".getBytes());
		Cipher aesDec= getCipherForBlock(Cipher.DECRYPT_MODE, blockID);
		System.out.println(new String(aesDec.doFinal(enc)));
		
		
	}
	
	private static Cipher getCipherForBlock(int opmode, int blockID) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException{
		BigInteger MODULUS = BigInteger.ONE.shiftLeft(BLOCK_SIZE * 8);
		/* Compute the index of the block to which data will be appended. */
		BigInteger block = BigInteger.valueOf(blockID);
		/* Add the block to the nonce to find the current counter. */
		BigInteger nonce = new BigInteger(1, iv);
		byte[] tmp = nonce.add(block).mod(MODULUS).toByteArray();
		/* Right-justify the counter value in a block-sized array. */
		byte[] ctr = new byte[BLOCK_SIZE];
		System.arraycopy(tmp, 0, ctr, BLOCK_SIZE - tmp.length, tmp.length);
		/* Use this to initialize the appending cipher. */
		IvParameterSpec newIvSpec = new IvParameterSpec(ctr);
		
		Cipher cipher= Cipher.getInstance(ENCRYPTION_ALGORITHM);
		MessageDigest digest = MessageDigest.getInstance(DIGEST);
		byte[] key=Arrays.copyOf(digest.digest((password).getBytes("UTF-8")),KEY_SIZE);
		cipher.init(opmode, new SecretKeySpec(key, ENCRYPTION_ALGORITHM.split("/")[0]),newIvSpec);
		return cipher;
	}
}
