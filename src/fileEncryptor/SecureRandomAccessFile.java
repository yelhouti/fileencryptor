package fileEncryptor;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.NullCipher;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.security.krb5.internal.crypto.dk.AesDkCrypto;

public class SecureRandomAccessFile {

	static final String DIGEST = "SHA-256";
	private final RandomAccessFile raf;
	final int BLOCK_PER_PAGE;
	final String ENCRYPTION_ALGORITHM;
	final int KEY_SIZE;
	private String password;
	private final byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private final BigInteger NONCE = new BigInteger(1, iv);
	BigInteger MODULUS;
	private final IvParameterSpec ivspec;
	Cipher cipherEnc;
	Cipher cipherDec;
	MessageDigest digest = MessageDigest.getInstance(DIGEST);
	byte[] key;
	final int AES_BLOCK_SIZE = 128 / 8;// AES block site in bytes
	private byte[] aesBuffer = new byte[AES_BLOCK_SIZE];

	public SecureRandomAccessFile(File file, String rights, int blockPerPage,
			String encryptionAlgorithm, String password, int keySize,
			int padding) throws IOException, NoSuchAlgorithmException,
			NoSuchPaddingException {
		// TODO Check BLOCKSIZE multiple of encryptionAlgorithm BLOCKSIZE
		raf = new RandomAccessFile(file, rights);
		Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
		BLOCK_PER_PAGE = blockPerPage;
		ENCRYPTION_ALGORITHM = encryptionAlgorithm;
		this.password = password;
		KEY_SIZE = keySize;
		file.createNewFile();// create file if not exist
		key = Arrays.copyOf(digest.digest((password).getBytes("UTF-8")),
				KEY_SIZE);
		MODULUS = BigInteger.ONE.shiftLeft(AES_BLOCK_SIZE * 8);
		ivspec = new IvParameterSpec(iv);
		cipherEnc = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		cipherDec = Cipher.getInstance(ENCRYPTION_ALGORITHM);
		SecretKeySpec keySpec = new SecretKeySpec(key,
				ENCRYPTION_ALGORITHM.split("/")[0]);
		try {
			cipherEnc.init(Cipher.ENCRYPT_MODE, keySpec, ivspec);
			cipherDec.init(Cipher.DECRYPT_MODE, keySpec, ivspec);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		raf.close();
	}

	public long length() throws IOException {
		return raf.length();
	}

	// FIXME set private
	public void setIV(BigInteger counter) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, UnsupportedEncodingException {
		byte[] tmp = NONCE.add(counter).mod(MODULUS).toByteArray();
		/* Right-justify the counter value in a block-sized array. */
		System.arraycopy(tmp, 0, iv, AES_BLOCK_SIZE - tmp.length, tmp.length);

		// return new NullCipher();
	}

	public byte[] readPages(int begin, int end, BigInteger[] keys)
			throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IOException, ShortBufferException {
		int nbrOfPages=end-begin;
		byte[] res= new byte[nbrOfPages * AES_BLOCK_SIZE*BLOCK_PER_PAGE];
		for (int i = 0; i < nbrOfPages; i++) {
			readPage(begin+i,keys[i],res,i*AES_BLOCK_SIZE*BLOCK_PER_PAGE);
		}
		return res;
	}

	public void readPage(int pageID, BigInteger key, byte[] outBuffer,
			int outOffset) throws IllegalBlockSizeException,
			BadPaddingException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException,
			IOException, ShortBufferException {
		raf.seek(pageID * AES_BLOCK_SIZE * BLOCK_PER_PAGE);
		raf.read(aesBuffer);
		setIV(key);
		cipherDec.doFinal(aesBuffer, 0, AES_BLOCK_SIZE, outBuffer, outOffset);
	}

	public void writeInPage(byte[] buffer, int pageID, BigInteger freshKey) throws IOException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException {
		setIV(freshKey);
		System.out.println("sraf: writing: freshKey: "+freshKey+"resulting IV:"+Arrays.toString(iv));
		raf.seek(pageID * BLOCK_PER_PAGE * AES_BLOCK_SIZE);
		byte[] enryptedPage=cipherEnc.doFinal(buffer);
		byte[] cipheriv=cipherEnc.getIV();
		System.arraycopy(iv, 0, cipheriv, 0, iv.length);
		System.out.println("cipher IV: "+Arrays.toString(cipherEnc.getIV())+" iv:"+Arrays.toString(iv));
		System.out.println("plain:"+Arrays.toString(buffer)+" enc:"+Arrays.toString(enryptedPage));
		raf.write(enryptedPage);
	}

	public void writePlain(byte[] plain) throws IOException {
		raf.write(plain);
	}

	public void seek(int pos) throws IOException {
		raf.seek(pos);
	}

	public int readPlainInt() throws IOException {
		return raf.readInt();
	}

	public void writePlainInt(int x) throws IOException {
		raf.writeInt(x);
	}

	public int getBLOCK_SIZE() {
		return AES_BLOCK_SIZE;
	}

	public long getFilePointer() throws IOException {
		return raf.getFilePointer();
	}

}
