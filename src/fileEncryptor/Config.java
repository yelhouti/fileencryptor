package fileEncryptor;

public final class Config {
	private Config(){
		
	}
	
	public static final int PAGE_SIZE=4*1024;//in bytes
	public static final String PASSWORD="123456789";
	public static final String ENCRYPTION_ALGORITHM="AES";
	public static final String DIGEST="SHA-1";
	public static final int KEY_SIZE=16;
}
