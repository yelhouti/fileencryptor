package fileEncryptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class TestCTR {
	public static void main(String[] args) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException {
		File toto = new File("toto.txt");
		File etoto = new File("etoto.txt");

		RandomAccessFile plainRaf = new RandomAccessFile(toto, "rw");
		byte[] plain = new byte[(int) toto.length()];
		plainRaf.readFully(plain);
		plainRaf.close();
		// System.out.println(new String(plain));
		System.out.println();

		SecureRandomAccessFile sraf = new SecureRandomAccessFile(etoto, "rw",
				16, "AES/CTR/NoPadding", "youssef", 16, 0);
		int nbrOfBlocks = (int) Math.ceil(toto.length() / (double) 16);
		byte[] buffer = new byte[16];
		for (int i = 0; i < nbrOfBlocks; i++) {
			// sraf.writeInBlock(Arrays.copyOfRange(plain, i*16,(1+i)*16), i);
		}
		// for (int i=0; i<nbrOfBlocks; i++){
		// System.out.print(new String(sraf.readBlock(i)));
		// }

		System.out.println("_____________________________________________");
		int blockId = 1;
		byte[] block = Arrays.copyOfRange(plain, blockId * 16,
				(blockId + 1) * 16);
		block[0] += 1;
		// sraf.writeInBlock(block, blockId);

		for (int i = 0; i < nbrOfBlocks; i++) {
			// System.out.print(new String(sraf.readBlocks(i,i+1)));
		}

	}
}
