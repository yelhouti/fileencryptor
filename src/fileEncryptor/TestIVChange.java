package fileEncryptor;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class TestIVChange {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		byte[] key = Arrays.copyOf("aaaa".getBytes(),	128/8);
		IvParameterSpec ivspec = new IvParameterSpec(iv);
		SecretKeySpec keySpec = new SecretKeySpec(key,"AES");

		Cipher cipherEnc = Cipher.getInstance("AES/CTR/NoPadding");
		cipherEnc.init(Cipher.ENCRYPT_MODE, keySpec, ivspec);
		System.out.println("cipher getiv1: "+Arrays.toString(cipherEnc.getIV()));
		iv[0]=1;
		System.out.println("ivspec: "+Arrays.toString(ivspec.getIV()));
		System.out.println("cipher getiv2: "+Arrays.toString(cipherEnc.getIV()));
	}

}
