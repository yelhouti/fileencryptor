package fileEncryptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Test3 {
	public static void main(String[] args) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidAlgorithmParameterException {
		File toto = new File("toto.txt");
		File etoto = new File("etoto.txt");
		File mtoto= new File("mtoto.txt");
		 etoto.delete();
		 etoto.createNewFile();
		 mtoto.delete();
		 mtoto.createNewFile();
		SecureRandomAccessFile sraf = new SecureRandomAccessFile(etoto, "rw",
				100, "AES/CTR/NoPadding", "youssef", 16, 0);

		EncryptedFileManager efm = new EncryptedFileManager(sraf, mtoto);
		//
		RandomAccessFile plainRaf = new RandomAccessFile(toto, "rw");
		byte[] plain = new byte[(int) toto.length()];
		plainRaf.readFully(plain);
		plainRaf.close();
		// System.out.println(new String(plain));
		System.out.println();

		 int nbrOfPages=(int) Math.ceil(toto.length()/(double)efm.PAGE_SIZE);
		 for (int i=0; i<nbrOfPages; i++){
		 int freeId=efm.getNextFreePageID();
		 int start=i*efm.PAGE_SIZE;
		 efm.writeInPage(Arrays.copyOfRange(plain,
		 start,start+efm.PAGE_SIZE),start, freeId);
		 }
		 System.out.println("update");
		 efm.updateFileMapInFile();
		 System.out.println("end update");
		 System.out.println("_____________________________________________");

//		byte[] recuperatedPlain = (efm.getPlainFromTo(0, (int) toto.length()));
//		System.out.println(new String(recuperatedPlain));
		// System.out.println(efm.getMapPagesIDs());
		// System.out.println(recuperatedPlain.length+" "+plain.length);
//		System.out.println(new String(recuperatedPlain)
//				.equals(new String(plain)));
	}
}
